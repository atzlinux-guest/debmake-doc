# Debmake documentation

This is the source file for debmake-doc package.

 * https://salsa.debian.org/debian/debmake-doc (upstream site)
 * https://www.debian.org/doc/devel-manuals#debmake-doc (build pages)
 * https://www.debian.org/doc/manuals/debmake-doc/index.en.html (English content)

All files included in this source tree are under the MIT license with its text
in the LICENSE file except for the files which explicitly claim differently in
them.

## Git repository

NOTE: use of "devel" branch is deprecated.

All updates should be committed to "main" branch of the Git repository at

  https://salsa.debian.org/debian/debmake-doc

The "main" branch is meant to build the non-native Debian binary package.  The
version number in debian/changelog is non-native version number.  This works
since this is also upstream repo.  For minor updates, we can use -2 revision,
too.  See "man dgit-maint-merge".

Test build:
    $ git deborig -f HEAD
    $ pdebuild

Playing with git purely from command line to track changes isn't easy.  Please
consider using GUI tool such as gitk.

When building revision packages such as -2, -3, ..., obtain and use the
original tar file used for -1 revision instead of using `git deborig -f HEAD`.

## Build system

The build system involves several `Makefile`s for make:

* Makefile -- "package" target does "git deborig -f HEAD; pdebuild"
* Makefile.pkg -- "logs" target to generate the packaging example logs

### Normal build

The packaging of debmake-doc is:

* package build by "make package" from the git "master" branch.
* upload package with "dput" (source only for Debian)

### Pre-build

Prior to the execution of "make package", the up-to-date packaging example
logs need to created as pre-build and committed to the source tree in the
testing environment.

 $ make logs
 $ git add -A .
 $ git commit -m "Update example logs"

This pre-build requires a testing environment with many packages related to
the typical source building infrastructure.

 * autoconf
 * automake
 * autopoint
 * cmake
 * debhelper
 * debmake
 * devscripts
 * faketime
 * gettext
 * gir1.2-gtk-3.0
 * libltdl-dev
 * libtool
 * libxml2-utils
 * lintian
 * locales-all | locales
 * python3-all
 * python3-debian
 * python3-distutils-extra
 * python3-gi
 * quilt
 * tree
 * vim
 * w3m
 * zip

### Debug pre-build for creating package build log examples

If you want to create only a particular version of packaging example logs for
debugging, please use DEBUG_VERSION.  For example, to build a example log for
2.0 version:

 $ make "DEBUG_VERSION=2.0" logs

### Debug build (English updates)

Update English original content by hacking ASCIIDOC text files under asciidoc/
directory.  The result can be previewed with the browser by generating html
files.

 $ make base-xml  # building base xml from asciidoc
 $ make LANGPO= all-xml   # building all xml source while updating PO from base-xml
 $ make LANGPO= css html  # preview html

Please note that LANGPO is set to empty string in the above to skip transltion.

Results are found under basedir/html/ .

If you get error free html file, you may wish to check PDF file.

 $ make LANGPO= pdf  # preview PDF

Results are found under basedir/ .

### Debug build (package build log examples and English updates)

If you are debugging ASCIIDOC markup issues with newly updated build log
examples and wish to test the source quickly without going through XML, here
is an alternative html build method:

 $ make logs
 $ cd asciidoc
 $ make

The result is asciidoc/debameke-doc.html

Please commit English asciidoc source text contents to git repository after
making sure all the above are bug free.

### Debug build (non-English updates)

We start almost same as "Debug build (English updates)".

Before working on updating translation, please make sure to refresh files
under po4a/po/ directory (a.k.a.  PO files) for your language <lang> from the
latest asciidoc text contents by test building if they weren't committed yet.

 $ make base-xml  # building base xml from asciidoc
 $ make LANGPO=<lang> all-xml   # building all xml source while updating PO from base-xml
 $ make LANGPO=<lang> css html  # building all html

or simply as:

 $ make build     # building all xml source while updating PO from asciidoc
 $ make LANGPO=<lang> css html  # building all html

Now you are ready to translate this source by hacking PO files for
LANGPO=<lang> under po4a/po/ directory using tools such as poedit.  The result
of your translation can be previewed with the browser by generating html
files.

 $ make LANGPO=<lang> css html  # preview html

Results are found under basedir/html/ .

If you get error free html file, you may wish to check PDF file.

 $ make LANGPO=<lang> pdf  # preview PDF

Results are found under basedir/ .

If satisfied, make sure to commit only your modified PO files to git "devel"
branch.  Assuming you are editing po/<lang>.po:

 $ git add po4a/po/<lang>.po
 $ git commit po4a/po/<lang>.po

### Translation in po4a/po directory

This directory contain po files for translators.

 >>> XXX FIXME XXX >>>
In order to keep the translation in sync with the English,
po4a/po/debmake-doc.pot is generated in the way not to contain <screen>
contents and url attribute of <ulink>.  This is done using po4a.
 <<< XXX FIXME XXX <<<

If you updated a PO file while others committing the same file before you
commit it, merging of the PO file isn't trivial.  Don't try to merge just by
git.  Here is the way to do it.

* Copy your latest `po4a/po/<lang>.po` to outside of the source tree as
  `/path/to/<lang>.po.my`.
* Use gitk to checkout local "master" branch.
* Use gitk to reset your local "master" to "remotes/origin/master" (hard reset).
* Use "git pull origin master"
* Move `po4a/po/<lang>.po` to `po/<lang>.po.their` .
* Execute
  `msguniq --use-first /path/to/<lang>.po.my /path/to/<lang>.po.their > po4a/po/<lang>.po`
* Update `po4a/po/<lang>.po` with the latest source by "make build"

Now you have merged PO to `po4a/po/<lang>.po`.

You may un-fuzzy manually and commit the clean tested `po4a/po/<lang>.po` to
git repo.

### Files in `debian-template/`

This directory contains a set of template files used to be copied to
`debian/*` files of many examples of the packaged source tree.

The changelog file in this debian-template/ is for the latest version of the
package example which is split up properly for each version and copied
accordingly.

### Files in `debhello-<version>/`

Each of these contains an entire set of an example source tree with properly
packaged `debian/*` files.

The upstream tarball used in the packaging example are generated by the tar
command while excluding the debian/ directory.

### Files in `debhello-<version>-pkg<revision>/`

These are used to generate the packaging process example.

These contain step<number>.cmd files which are executed sequentially and the
results are logged to the step<number>.log files with the script program.

Since the step<number>.log files are terminated by <CR><LF>, may contain
<TAB>, and may be too long, the filtered step<number>.slog files are generated.

The <revision> is usually 1 and it matches the Debian revision of the binary
packages generated.

If you wish to run script-by-script, you can do the following
 $ make step<number>.raw

Some cmd files running program which waits user input uses the expect command
to automate it to move forward.

### Generated `*.stamp` files

The existence of .stamp file in each directory indicates all the files in the
directory has been generated via Makefile and up-to-date.  This is not
committed to the VCS.

### Files in `asciidoc/`

This directory contains the source text files written in the asciidoc markup.
Their extension is usually .txt .

### Source upload

Make source package and record it to git:

    $ git git reset --hard HEAD
    $ git clean -d -f -x
    $ dpkg-buildpackage -S --no-sign
    $ pristine-tar ../debmake-doc_1.16.orig.tar.xz master
    $ cd ..
    $ debsign debmake-doc_1.16-1_source.changes
    $ dput debmake-doc_1.16-1_source.changes

Use of the `debsign` command is desirable to avoid picking old 1024 bit keys.

// vim:se tw=78 sts=4 ts=4 et ai:
